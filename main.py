print("hello")
num: int = 1
name: str = '1'
new_num = int(name)

print(type(new_num))

print('hi')
print('hello \nhow are you')

print("""\
line1
line2 
line3\
""")

s = ('ooooooooooooooooooooo'
     'nbbbbbbbbbbbbbbbbbbbbb')
print(s)

def datatypeTeller(data):
     print(type(data))

# datatype check
datatypeTeller(bool(1))
datatypeTeller(bool(0))
datatypeTeller(1)
datatypeTeller(2)
datatypeTeller({1,2,3,4})
datatypeTeller((1,3,4,7,7))
datatypeTeller([1,2,4,6,7])

# function
def practic(add):
     print(add)
     return add + 1


def everything(list):
     for i in list:
          if i > 2:
               print(str(i) + " is bigger than 2")

list = [1,2,3,4,5,6,7,8]
# 実行
everything(list)

class Car:
     a = "テスラ"
     def carStyle(self):
          #oldcarメソッド呼ぶ
          self.oldcar("ベンツ")
     def oldcar(self,oldcarname):
          print(self.a)
          print(oldcarname)

car = Car()
car.carStyle()


class Programs:
     # java で言う thisと同じ感じ
     def __init__(self,py,java,html):
          self.py = py
          self.java = java
          self.html = html

     def out(self):
          ai = self.py
          backend = self.java
          front = self.html
          list = [ai,backend,front]
          for i in list:
               print(i + " is programming language")

programs = Programs("python3.8", "Java se16", "HTML5")

programs.out()

word = "python "

print(word[:2])


str = 'My name is hoge. Nice to meet you guys'
## 前から何番前に指定した文字列があるか
print(str.find('hoge'))
## 後ろから何番目に指定した文字列があるか指定している
print(str.find('guys'))
## 指定した文字列が何回出現したか
print(str.count('hoge'))
## 先頭の文字列を大文字に変換
print(str.capitalize())
## 単語の先頭の文字を大文字に変換する
print(str.title())
## 単語をすべて小文字に
print(str.lower())
## 単語をすべて大文字に
print(str.upper())
## 大文字は小文字 小文字は大文字に変換
print(str.swapcase())
## すべての文字が大文字が判定
print(str.isupper())
## すべての文字が小文字が判定
print(str.islower())

print(str.replace('hoge','foo'))

f = "hoge"
l = "ホゲ男"
# "{}"置換フィールドに入れる文字列を指定
print('First={} Last={}'.format(f,l)) # First=hoge Last=ホゲ男

# "{}" 置換フィールドを引数番号による指定
# .formatカッコ内は左から(0,1,2,3....)という順番になる。
print('First={0} Last={1}'.format(f,l)) # First=hoge Last=ホゲ男
# 引数番号は自由に変更が可能
print('First={1} Last={0}'.format(f,l)) # First=ホゲ男 Last=hoge

# 3 変数を指定した展開
print('First={first} Last={last}'.format(first=f,last=l)) # First=ホゲ男 Last=hoge

# 4 新しいバージョン python3.6以降 処理が早い
print(f'First={f} Last={l}') # First=hoge Last=ホゲ男

l = [1,2,3,4,5,7,8,]
print(type(l))
n = [1,2,3,4,5,6]
n[::1]
for i in l:
     print(i)
     print(type(i))

# 文字列の扱い方終了
